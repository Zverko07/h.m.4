# Написать функцию которая принимает числа, выводит сумму чисел. Функцию надо вызвать.
def arr(klok1, klok2):
    print(klok1 + klok2)

arr(1,2)
# Написать функцию которая принимает числа, выводит разность чисел. Функцию надо вызвать.
def arr(klok1, klok2):
    print(klok1 - klok2)

arr(10,5)
# Написать функцию которая принимает числа, выводит произведение чисел. Функцию надо вызвать.
def arr(klok1, klok2):
    print(klok1 * klok2)

arr(2, 6)
# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.
def arr(klok1, klok2):
    print(klok1 / klok2)

arr(9, 3)
# Написать функцию которая принимает числа, выводит деление чисел. Функцию надо вызвать.
def arr(klok1, klok2):
    print(klok1 / klok2)

arr(16, 2)
# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.
arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

def i(ar):
    for job in ar:
        print(job, end=" ")

i(arr)
# Написать функцию которая принемает массив, проходится по циклом по массиву и печатает объекты массива. Функцию надо вызвать.
arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

def i(ar):
    for job in ar:
        print(job, end=" ")

i(arr)

# Напишите приммеры использования всех операций с массивами
    # len()
arr = [1, 2, 3, 5]
print(len(arr))
    # append()
arr = [1, 2, 3, 5]
arr.append(6)
print(arr)
    # clear()
arr = [1, 2, 3, 5]
arr.clear()
print(arr)

    # count()
arr = [1, 1, 2, 2, 3, 5]

print(arr.count(5))
    # copy()
arr = [1, 1, 2, 2, 3, 5]
b = arr.copy()
print(b)
print(b)
    # extend()
arr = [1, 1, 2, 2, 3, 5]
b = [5, 9]
arr.extend(b)
print(arr)
    # index()
a = ['c', 'v', 'd']
print(a.index('d'))
    # remove('Meder')
a = [1, 'Meder', 3]
a.remove('Meder')
print(a)
    # reverse()
a = [1, 2, 3]
a.reverse()
print(a)
    # pop()




# Оберните все операции в функции, которые принимают масссив и выполняют над нимм операцию. Функцию надо вызвать.
    # len()
a = [1, 2, 3]


def arr(go):
    print(a)


print(len(a))

    # append()
a = [1, 2, 3]


def arr(go):
    print(a)

a.append(7)
print(a)

    # clear()
a = [1, 2, 3]


def arr(go):
    print(a)

a.clear()
print(a)

    # count()
a = [1, 1, 2, 3, 3]


def arr(go):
    print(a)

print(a.count(3))

    # copy()
a = [1, 1, 2, 3, 3]


def arr(go):
    print(a)

print(a.count(2))

    # extend()
a = [1, 1, 2, 3, 3]

def arr(go):
    print(a)

b = [5,6]
a.extend(b)
print(a)

    # index()
a = [7, 9, 1, 2, 3, 3, 1]

def arr(go):
    print(a)


print(a.index(1))

    # remove()
a = [7, 9, 1, 2, 3, 3, 1]

def arr(go):
    print(a)

a.remove(3)
print(a)

    # reverse()
a = [7, 9, 1, 2, 3, 3, 1]

def arr(go):
    print(a)

a.reverse()
print(a)

    # pop()
a = [7, 9, 1, 2, 3, 3, 1]

def arr(go):
    print(a)

a.pop(3)
print(a)
